﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{

    [TestFixture]
    public class Main
    {
        public static String serviceName;
        public static String zmiennaKBazowa = "Zmienna z klasy bazowej";
        public static String inputPath,outputPath ;
        public static IWebDriver driver;
       

        [OneTimeSetUp]
        public void SetupSet() {

            Console.WriteLine("SetUp All Tests");
            inputPath = AppDomain.CurrentDomain.BaseDirectory;

            if (inputPath.Contains("workspace"))
            {
                inputPath = Directory.GetCurrentDirectory();
                outputPath = inputPath+"\\result";

                

            }  else {

                inputPath = inputPath.Replace("\\bin\\Debug", "") ;
                outputPath = inputPath;

            };
            
           
            JObject jOptions = JObject.Parse(File.ReadAllText(@inputPath + "\\options.json"));


            String service = (string)jOptions["service"];
            serviceName = service;
            Console.WriteLine(service);

             ChromeOptions options = new ChromeOptions();
             options.AddArguments("--start-maximized");         
             driver = new ChromeDriver(options);
             //driver.Manage().Timeouts().ImplicitWait  = TimeSpan.FromSeconds(20);
              




        }

        [OneTimeTearDown]
        public void tearDownSet() {

            int failed = TestContext.CurrentContext.Result.FailCount;
            JObject result = new JObject();
            if (failed == 0)
            {

                result.Add("status", "success");
                result.Add("result", "Service was ended with PASS status");
                JObject errors = new JObject();
                result.Add("errors", errors);

            }

            else {

                result.Add("status", "failed");
                result.Add("result", "Service was ended with FAILED status");
                JObject errors = new JObject();
                errors.Add("stackTrace", null);
                errors.Add("causedBy", null);
                errors.Add("screenshot", null);
                result.Add("errors", errors);

            }


            using (System.IO.StreamWriter file =
                 new System.IO.StreamWriter(outputPath+"\\result.json", false))
            {
                file.WriteLine(result.ToString());
            }

            driver.Quit();


        }



        [TearDown]
        public void TearDownTest()
        {

            Console.WriteLine("TearDown Test");

        }



        [SetUp]
        public void SetUpTest()
        {

            Console.WriteLine("SetUp Test");

        }




    }
}
